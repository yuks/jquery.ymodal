(function($) {
	$.yModal = function(element, options) {
		var defaults = {
			top : 100,
			overlay : 0.5,
			closeButton : null,
			onClose : function() {
			},
			onOpen : function() {
			}
		};
		var plugin = this;
		plugin.settings = {};
		var $element = $(element), element = element;
		plugin.init = function() {
			plugin.settings = $.extend({}, defaults, options);
		};
		plugin.open = function() {
			html = '<div id="overly"></div>';
			if ($('body').find('#overly').length != 0) {
				$('#overly').remove();
			}
			$('body').append(html);
			$('#overly').css({
				'position' : 'fixed',
				'z-index' : '9999',
				'top' : '0px',
				'left' : '0px',
				'height' : '100%',
				'width' : '100%',
				'background' : '#000',
				'display' : 'block',
				'opacity' : plugin.settings.overlay,
			});
			center_modal(element);
			plugin.settings.onOpen(element);
		};
		plugin.close = function() {
			$('#overly').remove();
			$(element).hide();
			plugin.settings.onClose(plugin);
		};
		var center_modal = function() {
			var modal_height = $(element).outerHeight();
			var modal_width = $(element).outerWidth();
			$('#overly').click(function() {
				plugin.close(element);
			});
			$(element).css({
				'display' : 'block',
				'position' : 'fixed',
				'z-index' : 99999999,
				'left' : 50 + '%',
				'margin-left' : -(modal_width / 2) + "px",
				'top' : plugin.settings.top + "px"
			});
		};
		plugin.init();
	};
	$.fn.yModal = function(options) {
		return this.each(function() {
			if (undefined == $(this).data('yModal')) {
				var plugin = new $.yModal(this, options);
				$(this).data('yModal', plugin);
			}
		});
	};
})(jQuery);